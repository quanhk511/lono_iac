# frozen_string_literal: true

require "roo"

module FuncommonHelper
  # common params
  def CreateParameter(array)
    array.each {
      |i| parameter(i.to_s)
    }
  end

  # crate output
  def CreateOutput(array)
    array.each {
      |i| output(i.to_s)
    }
  end

  # read file xlsx
  def readxlsx(file_path)
    xlsx = Roo::Spreadsheet.open(file_path, { expand_merged_ranges: true })
  end
end
