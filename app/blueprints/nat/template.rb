# frozen_string_literal: true

aws_template_format_version "2010-09-09"
description "NAT stack"

CreateParameter(@common_param)

####################################################
# PRASE DATA FROM EXCEL
####################################################

sheet_nat  = readxlsx("data/data.xlsx").sheet(3)
column_one = sheet_nat.column(1)
route_map  = sheet_nat.parse(
  nat: "NAT", 
  eip: "EIP", 
  subnet: "Subnet", 
  clean: true)

# ####################################################
# # CREATE NAT FOR DEV-ENV && STG-ENV
# ####################################################

route_map.each { |i|
  natgw_name = i[:nat].to_s
  eip_id     = i[:eip].to_s

  subnet_id     = i[:subnet].sub("1", "").split("-").map(&:capitalize).join("").to_s
  resource_name = natgw_name.sub("1", "").split("-").map(&:capitalize).join("").to_s
  parameter(subnet_id) # create param

  resource(
    resource_name, 
    Type: "AWS::EC2::NatGateway",
    Properties: {
      AllocationId: eip_id,
      SubnetId: ref(subnet_id),
      Tags: [
        { Key: "Name", Value: join("-", ref("ProjectName"), ref("StageName"), ref("SystemName"), natgw_name) }
      ].concat(@common_tags)
    })

  output(resource_name)
}
