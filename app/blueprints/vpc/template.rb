# frozen_string_literal: true

aws_template_format_version "2010-09-09"
description "VPC stack"

# Create common parameter
CreateParameter(@common_param)

####################################################
# PRASE DATA FROM EXCEL
####################################################

sheet_vpc  = readxlsx("data/data.xlsx").sheet(0)
column_one = sheet_vpc.column(1)
subnet_map = sheet_vpc.parse(
  subnet_name: "Subnet name", 
  cidr: "Cidr", 
  az: "AZ", 
  mode: "Mode", 
  clean: true)

####################################################
# CREATE VPC USE HELPER
####################################################

vpc_cidr          = column_one[1].to_s
resource_name_vpc = "VPC"

props = {
  CidrBlock: vpc_cidr
}
tags  = {
  Key: "Name", Value: join("-", ref("ProjectName"), ref("StageName"), ref("SystemName"), "vpc")
}

vpc(resource_name_vpc, props, tags)

####################################################
# CREATE SUBNET USE HELPER
####################################################

subnet_map.each { |i|
  subnet_name       = i[:subnet_name].to_s
  subnet_name_clear = subnet_name.sub("1", "").split("-").map(&:capitalize).join("")
  subnet_cidr       = i[:cidr].to_s
  resource_name     = subnet_name_clear.to_s
  az                = i[:az]

  props             = {
    CidrBlock: subnet_cidr,
    VpcId: ref(resource_name_vpc)
  }
  tags              = {
    Key: "Name",
    Value: join("-", ref("ProjectName"), ref("StageName"), ref("SystemName"), subnet_name) 
  }
  subnet(resource_name, az, props, tags)
  
  subnet_cidr_output = "Cidr" + resource_name
  output(
    subnet_cidr_output => {
      Value: subnet_cidr
    })
}
