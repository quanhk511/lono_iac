# frozen_string_literal: true

module VpcHelper
  def vpc(logical_id, props = {}, tags = {})
    default_props = {
      EnableDnsSupport: true,
      EnableDnsHostnames: true,
      InstanceTenancy: "default",
      Tags: [tags].concat(@common_tags)
    }
    props.reverse_merge!(default_props)

    resource(
      logical_id.to_s, 
      "AWS::EC2::VPC",
      props
      )

    resource(
      "InternetGateway", 
      "AWS::EC2::InternetGateway",
      props = {
        Tags: [
          Key: "Name", 
          Value: join("-", ref("ProjectName"), ref("StageName"), ref("SystemName"), "internet-gateway")
        ].concat(@common_tags)
      }
      )
    
    output("InternetGateway")

    resource(
      "VPCGatewayAttachment",
      "AWS::EC2::VPCGatewayAttachment",
      props = {
        VpcId: ref(logical_id.to_s),
        InternetGatewayId: ref("InternetGateway")
      }
      )
    output(
      logical_id.to_s
      )
  end

  def subnet(logical_id, az, props = {}, tags = {})
    default_props = {
      MapPublicIpOnLaunch: false,
      AvailabilityZone: select(az.to_s, get_azs("")),
      Tags: [tags].concat(@common_tags)
    }
    props.reverse_merge!(default_props)

    resource(
      logical_id.to_s, 
      "AWS::EC2::Subnet",
      props
      )
    
    output(
      logical_id.to_s
      )
  end
end
