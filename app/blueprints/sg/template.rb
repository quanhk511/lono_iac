# frozen_string_literal: true

aws_template_format_version "2010-09-09"
description "SG stack"

lst_param = [
  "VPC"
]

CreateParameter(@common_param.concat(lst_param))

####################################################
# PRASE DATA FROM EXCEL
####################################################

sheet_sg   = readxlsx("data/data.xlsx").sheet(1)
sg_map     = sheet_sg.parse(
  nu: "No", 
  security_group: "Security Group", 
  rule: "Rule", port: "Port", 
  source_destination: "Source/Destination", 
  protocol: "Protocol",
  clean: true)

####################################################
# CREATE SG USE HELPER
####################################################
sg_map.each { |i|
  numerical_order = i[:nu].to_s
  sg_name         = i[:security_group].to_s
  sg_name_clear   = sg_name.split("-").map(&:capitalize).join("")
  resource_name   = sg_name_clear.gsub(/\w\W*(\s|$)/) { |m| m.upcase }.to_s
  sg_regex        = "-sg"
  pl_regex        = "pl-"
  ip_regex        = "0.0.0.0/0"

  tags   = {
    Key: "Name",
    Value: join("-", format("%03d", numerical_order), ref("ProjectName"), ref("StageName"), ref("SystemName"), sg_name)
  }
  vpc_id = ref("VPC")

  sg(resource_name, vpc_id, tags)

  ####################################################
  # CREATE INGRESS USE HELPER
  ####################################################

  if i.has_value?("Inbound") && i[:source_destination].include?(sg_regex)

    source = i[:source_destination].split("-").map(&:capitalize).join("").gsub(/\w\W*(\s|$)/) { |m| m.upcase }.to_s
    
    resource_sg_ingress = resource_name + "Ingress" + source
    description         = "Ingress " + source
    sg_id               = resource_name
    port                = i[:port]

    props = {
      SourceSecurityGroupId: ref(source)
    }
    sg_ingress(resource_sg_ingress, sg_id, port, props, description)

  elsif i.has_value?("Inbound") && i[:source_destination].include?(pl_regex)

    source              = i[:source_destination].to_s
    resource_sg_ingress = resource_name + "Ingress" + "Prefix"
    description         = "Ingress " + "Prefix " + source
    sg_id               = resource_name
    port                = i[:port]

    props = {
      DestinationPrefixListId: source
    }
    sg_ingress(resource_sg_ingress, sg_id, port, props, description)

  elsif i.has_value?("Inbound") && i.has_value?(ip_regex)

    source              = i[:source_destination].to_s
    resource_sg_ingress = resource_name + "Ingress" + "OpenToWorld"
    description         = "Ingress " + "with cidr open to world"
    sg_id               = resource_name
    port                = i[:port]

    props = {
      CidrIp: source
    }
    sg_ingress(resource_sg_ingress, sg_id, port, props, description)

  elsif i.has_value?("Inbound")

    source              = i[:source_destination].to_s
    resource_sg_ingress = resource_name + "Ingress" + "SpecificIp"
    description         = "Ingress " + "Specific Ip " + source
    sg_id               = resource_name
    port                = i[:port]

    props = {
      CidrIp: source
    }
    sg_ingress(resource_sg_ingress, sg_id, port, props, description)
  end

  ####################################################
  # CREATE ENGRESS USE HELPER
  ####################################################

  if i.has_value?("Outbound") && i[:source_destination].include?(sg_regex)

    destination        = i[:source_destination].split("-").map(&:capitalize).join("").gsub(/\w\W*(\s|$)/) { |m| m.upcase }.to_s
    resource_sg_egress = resource_name + "Egress" + destination
    description        = "Egress " + destination
    sg_id              = resource_name
    port               = i[:port]

    props = {
      DestinationSecurityGroupId: ref(destination)
    }
    sg_egress(resource_sg_egress, sg_id, port, props, description)

  elsif i.has_value?("Outbound") && i[:source_destination].include?(pl_regex)

    destination        = i[:source_destination].to_s
    resource_sg_egress = resource_name + "Egress" + "Prefix"
    description        = "Egress " + "Prefix " + destination
    sg_id              = resource_name
    port               = i[:port]

    props = {
      DestinationPrefixListId: destination
    }
    sg_egress(resource_sg_egress, sg_id, port, props, description)

  elsif i.has_value?("Outbound") && i.has_value?(ip_regex)

    destination        = i[:source_destination].to_s
    resource_sg_egress = resource_name + "Egress" + "AwsServices"
    description        = "Egress " + "AWS Services"
    sg_id              = resource_name
    port               = i[:port]

    props = {
      CidrIp: destination
    }
    sg_egress(resource_sg_egress, sg_id, port, props, description)

  elsif i.has_value?("Outbound")

    destination        = i[:source_destination].to_s
    resource_sg_egress = resource_name + "Egress" + "SpecificIp"
    description        = "Egress " + "Specific Ip " + destination
    sg_id              = resource_name
    port               = i[:port]

    props = {
      CidrIp: destination
    }
    sg_egress(resource_sg_egress, sg_id, port, props, description)
  end
}
