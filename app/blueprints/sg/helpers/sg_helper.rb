# frozen_string_literal: true

module SgHelper
  def sg(logical_id, vpc_id, tags = {})
    default_props = {
      VpcId: vpc_id,
      GroupDescription: logical_id.to_s,
      Tags: [tags].concat(@common_tags)
    }

    resource(
      logical_id.to_s, 
      "AWS::EC2::SecurityGroup",
      default_props
      )

    output(
      logical_id.to_s
      )
  end

  def sg_ingress(logical_id, sg_id, port, props = {}, description)
    default_props = {
      GroupId: ref(sg_id.to_s),
      Description: description.to_s,
      IpProtocol: "TCP",
      FromPort: port.to_s,
      ToPort: port.to_s  
    }
    props.reverse_merge!(default_props)

    resource(
      logical_id.to_s, 
      "AWS::EC2::SecurityGroupIngress",
      props
      )
  end

  def sg_egress(logical_id, sg_id, port, props = {}, description)
    default_props = {
      GroupId: ref(sg_id.to_s),
      Description: description.to_s,
      IpProtocol: "TCP",
      FromPort: port.to_s,
      ToPort: port.to_s  
    }
    props.reverse_merge!(default_props)

    resource(
      logical_id.to_s, 
      "AWS::EC2::SecurityGroupEgress",
      props
      )
  end
end
