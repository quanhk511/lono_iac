# frozen_string_literal: true

# common tag
@common_tags = [
  { Key: ref("CostCenterKey"), Value: ref("CostCenterValue") },
  { Key: ref("EnvironmentKey"), Value: ref("EnvironmentValue") },
  { Key: ref("ProjectKey"), Value: ref("ProjectValue") },
  { Key: ref("ProjectOwnerKey"), Value: ref("ProjectOwnerValue") }
]

# common params
@common_param = [
  "StageName", "ProjectName", "SystemName", "CostCenterKey",
  "CostCenterValue", "EnvironmentKey", "EnvironmentValue",
  "ProjectKey", "ProjectValue", "ProjectOwnerKey", "ProjectOwnerValue"
]
