# frozen_string_literal: true

aws_template_format_version "2010-09-09"
description "VPC flow log stack"

lst_param = [
  "VPC"
]

CreateParameter(@common_param.concat(lst_param))

parameter(
  "TrafficType",
  Default: "REJECT",
  AllowedValues: [ 
    "ACCEPT", 
    "REJECT", 
    "ALL" 
  ]
  )

####################################################
# CREATE FLOW  LOG DELEVERED TO S3
####################################################

resource(
  "LogBucket", 
  Type: "AWS::S3::Bucket",
  Properties: {
    BucketName: join("-", ref("ProjectName"), ref("StageName"), ref("SystemName"), "flow-log"),
    PublicAccessBlockConfiguration: {
      BlockPublicAcls: true,
      BlockPublicPolicy: true,
      IgnorePublicAcls: true,
      RestrictPublicBuckets: true
    },
    BucketEncryption: {
      ServerSideEncryptionConfiguration: [
        ServerSideEncryptionByDefault: {
          SSEAlgorithm: "AES256"
        }
      ]
    },
    Tags: @common_tags
  }
  )

resource(
  "FlowLogInternalBucket",
  Type: "AWS::EC2::FlowLog",
  Properties: {
    LogDestination: get_att("LogBucket.Arn"),
    LogDestinationType: "s3",
    ResourceId: ref("VPC"),
    ResourceType: "VPC",
    TrafficType: ref("TrafficType"),
    Tags: [
      { Key: "Name", Value: join("-", ref("ProjectName"), ref("StageName"), ref("SystemName"), "flow-log-s3") }
    ].concat(@common_tags)
  }
)

####################################################
# CREATE FLOW  LOG DELEVERED TO CLOUDWATCH
####################################################
# resource(
#   "LogGroup",
#   Type: "AWS::Logs::LogGroup",
#   Properties: {
#     RetentionInDays: 30,
#     KmsKeyId: ref("FlowlogKMS"),
#     Tags: [
#       { Key: "Name", Value: join("-", ref("ProjectName"), ref("StageName"), ref("SystemName"), "vpc-flow-log") }
#     ].concat(@common_tags)
#   }
#   )

# resource(
#   "FlowLogCloudwatch",
#   Type: "AWS::EC2::FlowLog",
#   Properties: {
#     DeliverLogsPermissionArn: get_att("FlowLogRole.Arn"),
#     LogDestinationType: "cloud-watch-logs",
#     ResourceType: "VPC",
#     LogGroupName: ref("LogGroup"),
#     ResourceId: ref("VPC"),
#     TrafficType: ref("TrafficType"),
#     Tags: [
#       { Key: "Name", Value: join("-", ref("ProjectName"), ref("StageName"), ref("SystemName"), "flow-log-cw") }
#     ].concat(@common_tags)
#   }
# )

# resource(
#   "FlowLogRole", 
#   Type: "AWS::IAM::Role",
#   Properties: {
#     AssumeRolePolicyDocument: {
#       Version: "2012-10-17",
#       Statement: [
#         {
#           Effect: "Allow",
#           Principal: {
#             Service: [
#               "vpc-flow-logs.amazonaws.com"
#             ]
#           },
#           Action: [
#             "sts:AssumeRole"
#           ]
#         }
#       ]
#     },
#     Policies: [
#       {
#         "PolicyName": "flowlogs-policy",
#         "PolicyDocument": {
#           "Version": "2012-10-17",
#           "Statement": [
#                 {
#                   "Effect": "Allow",
#                   "Action": [
#                     "logs:CreateLogStream",
#                     "logs:PutLogEvents",
#                     "logs:DescribeLogGroups",
#                     "logs:DescribeLogStreams"
#                   ],
#                   "Resource": get_att("LogGroup.Arn")
#                 }
#             ]
#         }        
#       }
#     ],
#     Tags: @common_tags
#   })
