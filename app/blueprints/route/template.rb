# frozen_string_literal: true

aws_template_format_version "2010-09-09"
description "Route stack"

lst_param = [
  "VPC",
]

CreateParameter(@common_param.concat(lst_param))

####################################################
# PRASE DATA FROM EXCEL
####################################################

sheet_route  = readxlsx("data/data.xlsx").sheet(2)
column_one   = sheet_route.column(1)
column_three = sheet_route.column(3)
route_map    = sheet_route.parse(
  route_table: "Route Table", 
  association: "Association",
  route_1: "Route 1", 
  route_2: "Route 2", 
  clean: true)

####################################################
# CREATE ROUTE TABLE IG + ROUTE IG
####################################################

route_table_ig   = column_one[1].to_s
resource_name_rt = route_table_ig.split("-").map(&:capitalize).join("")
resource_name_r  = resource_name_rt + "Route"
gateway_id       = column_three[1].split("-").map(&:capitalize).join("").to_s
vpc_id           = ref("VPC")

# create rt
tags = {
  Key: "Name",
  Value: join("-", ref("ProjectName"), ref("StageName"), ref("SystemName"), route_table_ig)
}

route_table(resource_name_rt, vpc_id, tags)

# create route
props = {
  GatewayId: ref(gateway_id)
}
parameter(gateway_id) # create param
route(resource_name_r, resource_name_rt, props)

# create association
route_map.each { |i|
  subnet_id        = i[:association].sub("1", "").split("-").map(&:capitalize).join("")
  resource_name_as = "IgAssociation" + subnet_id

  if i.has_value?(route_table_ig)
    parameter(subnet_id) # create param
    rt_association(resource_name_as, subnet_id, resource_name_rt)
  end
}

####################################################
# CREATE ROUTE NAT
####################################################

route_table_nat  = column_one[3].to_s
resource_name_rt = route_table_nat.split("-").map(&:capitalize).join("")
resource_name_r  = resource_name_rt + "Route"
nat_id           = column_three[3].sub("1", "").split("-").map(&:capitalize).join("").to_s

# create rt
tags = {
  Key: "Name",
  Value: join("-", ref("ProjectName"), ref("StageName"), ref("SystemName"), route_table_nat)
}
route_table(resource_name_rt, vpc_id, tags)

# create route
props = {
  NatGatewayId: ref(nat_id)
}
parameter(nat_id) # create param
route(resource_name_r, resource_name_rt, props)

# create association
route_map.each { |i|
  subnet_id        = i[:association].sub("1", "").split("-").map(&:capitalize).join("")
  resource_name_as = "NatAssociation" + subnet_id

  if i.has_value?(route_table_nat)
    parameter(subnet_id) # create param
    rt_association(resource_name_as, subnet_id, resource_name_rt)
  end
}

####################################################
# CREATE ROUTE TABLE ENDPOINT
####################################################

route_table_ep   = column_one[17].to_s
resource_name_rt = route_table_ep.split("-").map(&:capitalize).join("")
resource_name_r  = resource_name_rt + "Route"

# create rt
tags = {
  Key: "Name",
  Value: join("-", ref("ProjectName"), ref("StageName"), ref("SystemName"), route_table_ep)
}
route_table(resource_name_rt, vpc_id, tags)

# create association
route_map.each { |i|
  subnet_id        = i[:association].sub("1", "").split("-").map(&:capitalize).join("")
  resource_name_as = "EndpointAssociation" + subnet_id

  if i.has_value?(route_table_ep)
    parameter(subnet_id) # create param
    rt_association(resource_name_as, subnet_id, resource_name_rt)
  end
}

####################################################
# CREATE ROUTE TABLE NAT + ENDPOINT
####################################################

route_table_nat_ep = column_one[13].to_s
resource_name_rt   = route_table_nat_ep.split("-").map(&:capitalize).join("")
resource_name_r    = resource_name_rt + "Route"
nat_id             = column_three[13].sub("1", "").split("-").map(&:capitalize).join("").to_s

# create rt
tags = {
  Key: "Name",
  Value: join("-", ref("ProjectName"), ref("StageName"), ref("SystemName"), route_table_nat_ep)
}
route_table(resource_name_rt, vpc_id, tags)

# create route
props = {
  NatGatewayId: ref(nat_id)
}
parameter(nat_id) # create param
route(resource_name_r, resource_name_rt, props)

# create association
route_map.each { |i|
  subnet_id        = i[:association].sub("1", "").split("-").map(&:capitalize).join("")
  resource_name_as = "NatEndpointAssociation" + subnet_id

  if i.has_value?(route_table_nat_ep)
    parameter(subnet_id) # create param
    rt_association(resource_name_as, subnet_id, resource_name_rt)
  end
}
