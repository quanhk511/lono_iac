# frozen_string_literal: true

module RouteHelper
  def route_table(logical_id, vpc_id, tags = {})
    default_props = {
      VpcId: vpc_id,
      Tags: [tags].concat(@common_tags)
    }

    resource(
      logical_id.to_s, 
      "AWS::EC2::RouteTable",
      default_props
      )

    output(
      logical_id.to_s
      )
  end

  def route(logical_id, rt_id, props = {})
    default_props = {
      RouteTableId: ref(rt_id.to_s),
      DestinationCidrBlock: "0.0.0.0/0"
    }
    
    props.reverse_merge!(default_props)

    resource(
      logical_id.to_s, 
      "AWS::EC2::Route",
      props
      )
  end

  def rt_association(logical_id, subnet_id, rt_id)
    default_props = {
      SubnetId: ref(subnet_id.to_s),
      RouteTableId: ref(rt_id.to_s)
    }
    
    resource(
      logical_id.to_s, 
      "AWS::EC2::SubnetRouteTableAssociation",
      default_props
      )
  end
end
