# frozen_string_literal: true

aws_template_format_version "2010-09-09"
description "Route stack"

lst_param = [
  "VPC",
]

CreateParameter(@common_param.concat(lst_param))

####################################################
# PRASE DATA FROM EXCEL
####################################################

sheet_nacl = readxlsx("data/data.xlsx").sheet(5)
nacl_map   = sheet_nacl.parse(
  nacl_name: "Nacl", 
  association: "Association", 
  rule: "Rule", 
  rule_num: "Rule Number", 
  port_from: "Port From", 
  port_to: "Port To", 
  cidr: "Cidr", 
  clean: true)

####################################################
# CREATE NACL AND ASSOCIATION
####################################################
nacl_map.each { |i|
  nacl_name          = i[:nacl_name].to_s
  association_subnet = i[:association].to_s
  nacl_name_clear    = nacl_name.split("-").map(&:capitalize).join("").to_s
  resource_name      = nacl_name_clear

  resource(
    resource_name,
    Type: "AWS::EC2::NetworkAcl",
    Properties: {
      VpcId: ref("VPC"),
      Tags: [
        { 
          Key: "Name",
          Value: join("-", ref("ProjectName"), ref("StageName"), ref("SystemName"), nacl_name) 
        }
      ].concat(@common_tags)
    })

  # Network ACL Association
  nacl_id       = resource_name
  subnet_id_ary = association_subnet.split(" ")
  nacl_association(subnet_id_ary, nacl_id)

  ####################################################
  # CREATE RULE INGRESS USE HELPER
  ####################################################

  subnet_cidr_regex = "subnet"
  cidr              = i[:cidr].to_s
  rule_number       = i[:rule_num].to_s
  from              = i[:port_from]
  to                = i[:port_to]

  if i.has_value?("Inbound") && cidr.include?(subnet_cidr_regex)

    subnet_cidr = "Cidr" + cidr.sub("1", "").split("-").map(&:capitalize).join("")

    props = {
      NetworkAclId: ref(resource_name),
      RuleNumber: rule_number,
      CidrBlock: ref(subnet_cidr)
    }

    parameter(subnet_cidr) # create param
    logical_id = resource_name + "InboundRule" + rule_number.last()

    nacl_ingress(logical_id, props, from, to)

  elsif i.has_value?("Inbound")

    props = {
      NetworkAclId: ref(resource_name),
      RuleNumber: rule_number,
      CidrBlock: cidr
    }

    logical_id = resource_name + "InboundRule" + rule_number.last()

    nacl_ingress(logical_id, props, from, to)
  end

  ####################################################
  # CREATE RULE EGRESS USE HELPER
  ####################################################

  if i.has_value?("Outbound") && cidr.include?(subnet_cidr_regex)

    subnet_cidr = "Cidr" + cidr.sub("1", "").split("-").map(&:capitalize).join("")

    props = {
      NetworkAclId: ref(resource_name),
      RuleNumber: rule_number,
      CidrBlock: ref(subnet_cidr)
    }

    parameter(subnet_cidr) # create param
    logical_id = resource_name + "OutboundRule" + rule_number.last()

    nacl_egress(logical_id, props, from, to)

  elsif i.has_value?("Outbound")

    props = {
      NetworkAclId: ref(resource_name),
      RuleNumber: rule_number,
      CidrBlock: cidr
    }

    logical_id = resource_name + "OutboundRule" + rule_number.last()

    nacl_egress(logical_id, props, from, to)
  end
}
