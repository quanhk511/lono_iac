# frozen_string_literal: true

module NaclHelper
  def nacl_egress(logical_id, props = {}, from, to)
    default_props = {
      Protocol: 6,
      Egress: true,
      RuleAction: "allow",
      PortRange: {
        From: from.to_i,
        To: to.to_i        
      }
    }
    props.reverse_merge!(default_props)

    resource(
      logical_id.to_s, 
      "AWS::EC2::NetworkAclEntry",
      props
      )
  end

  def nacl_ingress(logical_id, props = {}, from, to)
    default_props = {
      Protocol: 6,
      RuleAction: "allow",
      PortRange: {
        From: from.to_i,
        To: to.to_i        
      }
    }
    props.reverse_merge!(default_props)

    resource(
      logical_id.to_s, 
      "AWS::EC2::NetworkAclEntry",
      props
      )
  end

  def nacl_association(subnet_id_ary = [], nacl_id)
    subnet_id_ary.length.times do |i|

      subnet_id       = subnet_id_ary[i]
      subnet_id_clear = subnet_id.sub("1", "").split("-").map(&:capitalize).join("").to_s
      parameter(subnet_id_clear) # create param

      default_props = {
        SubnetId: ref(subnet_id_clear),
        NetworkAclId: ref(nacl_id.to_s)
      }

      logical_id = "NaclAssociation" + subnet_id_clear
      
      resource(
        logical_id, 
        "AWS::EC2::SubnetNetworkAclAssociation",
        default_props
        )
    end
  end
end
