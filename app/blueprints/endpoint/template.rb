# frozen_string_literal: true

aws_template_format_version "2010-09-09"
description "NAT stack"

lst_param = [
  "VPC"
]

CreateParameter(lst_param)

####################################################
# PRASE DATA FROM EXCEL
####################################################

sheet_ep      = readxlsx("data/data.xlsx").sheet(4)
column_one    = sheet_ep.column(1)
column_tow    = sheet_ep.column(2)
resource_name = column_one[1].to_s
rt_1          = column_tow[1].split("-").map(&:capitalize).join("").to_s
rt_2          = column_tow[2].split("-").map(&:capitalize).join("").to_s
rt_3          = column_tow[3].split("-").map(&:capitalize).join("").to_s

####################################################
# CREATE VPC ENDPOINT S3
####################################################

CreateParameter([rt_1, rt_2, rt_3]) # create param
resource(
  resource_name, 
  Type: "AWS::EC2::VPCEndpoint",
  Properties: {
    ServiceName: sub("com.amazonaws.${AWS::Region}.s3"),
    VpcId: ref("VPC"),
    RouteTableIds: [
      ref(rt_1),
      ref(rt_2),
      ref(rt_3)
    ],
    PolicyDocument: {
      Version: "2012-10-17",
      Statement: [{
        Effect: "Allow",
        Principal: "*",
        Action: "*",
        Resource: "*"
      }]
    }
  })

output("VPCEndpointS3")
