class AllowRegions
  def call(blueprint)
    ["us-west-1"]
  end
end

Lono.configure do |config|
  config.allow.regions = AllowRegions
  config.logger.level = "info"
  config.layering.show = true
end
