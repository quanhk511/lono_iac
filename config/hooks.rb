# frozen_string_literal: true

require "yaml"
require "json"

module SecurityCheck
  def securitycheck
    "Security check by cfn_nag..."    
  end

  def cfn_nag
    "cfn_nag output/*/template.yml"
  end
end

module Lint
  def linting
    "Lint by cfn-lint..."    
  end
  
  def cfn_lint
    "cfn-lint output/*/template.yml"
  end
end

module GenTaskcat
  def testingCFN
    "test CFN with taskcat..."
  end

  def dirBlueprints
    Dir.chdir("./output") {
      Dir["*"].reject { |o| 
        not File.directory?(o)
      }
    }
  end

  def initParams
    @parameters  = Hash.new
    dirblueprint = dirBlueprints[0]
    data_hash    = JSON.parse(File.read("output/#{dirblueprint}/params.json"))
    data_hash.each { |couple|
      @parameters["#{couple['ParameterKey']}"] = couple["ParameterValue"]
    }
    @parameters
  end

  def addParams
    @parameters  = initParams
    dirblueprint = dirBlueprints[0]
    yamlTemp     = %(
---
project:
  name: taskcat-run-test
  regions:
    - us-west-1
tests:
  lono:
    template: output/#{dirblueprint}/template.yml
)
    hashYaml                                = YAML.load(yamlTemp)
    hashYaml["tests"]["lono"]["parameters"] = @parameters
    hashYaml
  end

  def writeFile
    @hash = addParams
    File.open(".taskcat.yml", "w") { |f| 
      f.write @hash.to_yaml
    }
  end
end

class Hook
  include Lint
  include SecurityCheck
  include GenTaskcat
  def call
    puts linting
    system(cfn_lint)
    puts securitycheck
    system(cfn_nag)
    puts testingCFN
    writeFile()
  end
end

after(
  "build",
  execute: Hook,
)
