# Install ruby for mac
Ref: https://gorails.com/setup/osx/11-big-sur  

# Install lono
```bash
gem install lono --prerelease
    lono new shim
```

# Prerequisites
```bash
❯ ruby -v
ruby 3.1.0p0 (2021-12-25 revision fb4df44d16) [x86_64-darwin21]

❯ lono -v
Lono: 8.0.0-rc4

❯ bundler -v
Bundler version 2.3.10
```

# Inital new project
```bash
lono new project infra_project
    cd infra_project
        lono new blueprint demo
            lono summary demo
```
and then add "Standard - Ruby style guide, linter, and formatter"   
Install Standard by adding it to your Gemfile and running bundle install:   
```
gem "standard", group: [:development, :test]
```
after you run command   
```bash
bundle exec standardrb
```
# Inital exist project
```bash
bundle install
    bundle exec lono new shim
    source ~/.zshrc
```

## Deploy
```bash
lono up vpc --yes
    lono up sg --yes
    lono up nat --yes
        lono up route --yes
            lono up endpoint --yes
```